# 工程模板

* 默认编译生成静态库
* 默认关闭 module 相关功能，不支持被 Swift 引用。可手工在 Build Settings 中开启。
* 默认提供 XXX.bundle，XXX.xcassets 两种资源目录，不建议再将资源独立放置。
* 默认添加 XXX-Prefix.pch 公共头文件。
* 默认最低支持 iOS 8.0。
* 默认添加 XXX-Aggregate 目标，可以构建包含四个架构的 Fat Binary。

# 关于资源

* XXX.bundle 为空目录，Git 管理可能有问题，如果不需要可以删除。
* 小图片资源建议添加到 xcassets 中管理，不需要 1x 图片。（大图片会被 imageNamed 缓存，不建议使用 xcassets 管理）
* 如果使用 xcassets，在 podspec 中不能使用 `**/*.*` 的方式包含，因为 xcassets 本身就是个目录。其它独立资源可以使用 `**/*.*`。
* 在主工程中，Cocoapods 会自动合并多个 xcassets。
* 工程的 Build Phase 中默认添加了 Copy Assets 阶段用于拷贝 xcassets 到 Framework 目录，如果不使用 xcassets 可以一起删除。
* 工程将 xcassets 从 Copy Bundle Resources 中移除（如果仍在 Copy Bundle Resources 会被编译成，在主工程中就无法合并了）。

# 关于 Aggregate 构建

* 如果工程有依赖，必须使用 workspace 构建，可以将 Aggregate 的脚本换成如下：
```shell
#!/bin/sh

export LANG=en_US.UTF-8



# capture ERR signal

trap 'exit 2' ERR



cd "$SRCROOT"



PROJECT=`ls .|grep xcodeproj|cut -d. -f1`

WORKSPACE=`ls .|grep xcworkspace|cut -d. -f1`

SRCROOT=.



BUILD_TYPE=""

if [[ -n "$WORKSPACE" ]]; then

echo "Workspace name ${WORKSPACE}"

BUILD_TYPE="-workspace ${PROJECT}.xcworkspace"

else

echo "Project name ${PROJECT}"

BUILD_TYPE="-project ${PROJECT}.xcodeproj"

fi



echo "Building configuration release."



TEMP_WORKING_DIR=$(mktemp -d -t mpaaskit);

BUILT_DIR_iphoneos=$TEMP_WORKING_DIR/iphoneos;

BUILT_DIR_iphonesimulator=$TEMP_WORKING_DIR/iphonesimulator;

echo "BUILT_DIR_iphoneos: $BUILT_DIR_iphoneos";

echo "BUILT_DIR_iphonesimulator: $BUILT_DIR_iphonesimulator";

mkdir -p "$BUILT_DIR_iphoneos";

mkdir -p "$BUILT_DIR_iphonesimulator";



PREPROCESSOR_DEFINITIONS='$(inherited) BUILD_FOR_MPAAS BUILD_MPAAS_AS_SEPARATE_FRAMEWORK'



# The products directory will be creates when building the project.

PRODUCT_DIR=Products

rm -rf "$PRODUCT_DIR"

[[ ! -d "$PRODUCT_DIR" ]] && { mkdir -m 777 -pv "$PRODUCT_DIR"; }



# Building both architectures.

set -o history -o histexpand

ARCHS="armv7 arm64"

ARCHS_CMD="-arch armv7 -arch arm64"

xcodebuild GCC_PREPROCESSOR_DEFINITIONS="${PREPROCESSOR_DEFINITIONS}" VALID_ARCHS="${ARCHS}" CONFIGURATION_BUILD_DIR="$BUILT_DIR_iphoneos" clean build ${BUILD_TYPE} -configuration "Release" -scheme "${PROJECT}" -sdk iphoneos ${ARCHS_CMD}

BUILD_SETTINGS=$(!! -showBuildSettings 2>/dev/null)

eval "$(echo "$BUILD_SETTINGS" | grep "BUILT_PRODUCTS_DIR\|WRAPPER_EXTENSION" | grep " = " | sed "s/ = /=/g" | sed "s/    //g")"

DEVICE_FRAMEWORK=${BUILT_PRODUCTS_DIR}/${PROJECT}.${WRAPPER_EXTENSION}



set -o history -o histexpand

ARCHS="i386 x86_64"

ARCHS_CMD="-arch i386 -arch x86_64"

xcodebuild GCC_PREPROCESSOR_DEFINITIONS="${PREPROCESSOR_DEFINITIONS}" VALID_ARCHS="${ARCHS}" CONFIGURATION_BUILD_DIR="$BUILT_DIR_iphonesimulator" clean build ${BUILD_TYPE} -configuration "Release" -scheme "${PROJECT}" -sdk iphonesimulator ${ARCHS_CMD}

BUILD_SETTINGS=$(!! -showBuildSettings 2>/dev/null)

eval "$(echo "$BUILD_SETTINGS" | grep "BUILT_PRODUCTS_DIR\|WRAPPER_EXTENSION" | grep " = " | sed "s/ = /=/g" | sed "s/    //g")"

SIMULATOR_FRAMEWORK=${BUILT_PRODUCTS_DIR}/${PROJECT}.${WRAPPER_EXTENSION}



# Copies the headers files to the final product folder.

cp -r "${DEVICE_FRAMEWORK}" "${PRODUCT_DIR}"



# Uses the Lipo Tool to merge both binary files (i386 + armv7) into one Universal final product.

echo ""

rm -f "${PRODUCT_DIR}/${PROJECT}.${WRAPPER_EXTENSION}/${PROJECT}"

echo "Use 'lipo' to create fat binary."

echo "Device framework path:\n${DEVICE_FRAMEWORK}/${PROJECT}"

echo "Simulator framework path:\n${SIMULATOR_FRAMEWORK}/${PROJECT}"

echo "Output fat framework path:\n${PRODUCT_DIR}/${PROJECT}.${WRAPPER_EXTENSION}/${PROJECT}"

lipo -create "${DEVICE_FRAMEWORK}/${PROJECT}" "${SIMULATOR_FRAMEWORK}/${PROJECT}" -output "${PRODUCT_DIR}/${PROJECT}.${WRAPPER_EXTENSION}/${PROJECT}"



echo "\nDone."

open "${PRODUCT_DIR}"
```
