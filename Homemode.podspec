Pod::Spec.new do |s|

s.name         = "Homemode"
s.version      = "${version}"
s.summary      = "Homemode类库"

s.description  = <<-DESC
* Detail about Homemode framework.
DESC

s.homepage     = "http://"
s.license      = 'MIT (example)'
s.author       = { "未定义" => "undefined" }
s.platform     = :ios, '8.0'
s.ios.deployment_target = '8.0'
s.source       = { :http => '${url}' }
s.preserve_paths = "Homemode.framework/*"
s.resources  = "Homemode.framework/*.{bundle,xcassets}"
s.vendored_frameworks = 'Homemode.framework'
s.requires_arc = true
s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Homemode' }

#s.dependency 'XXXX'

end
